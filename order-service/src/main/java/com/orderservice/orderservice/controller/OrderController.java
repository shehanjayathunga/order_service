package com.orderservice.orderservice.controller;

import com.orderservice.orderservice.dto.OrderDTO;
import com.orderservice.orderservice.service.OrderService;
import feign.Body;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/order")
public class OrderController {

    @Autowired
    private OrderService service;

    //get order record data using id
    @GetMapping("/getOrderByUserId/{id}")
    public List<OrderDTO>getOrderByUserId(@PathVariable final Long id){
        return service.getOrderByUserId(id);

    }

    //create new order record
    @PostMapping("/createOrder")
    public boolean createOrder(@RequestBody OrderDTO order){
        return service.createOrder(order);

    }


    //get all orders
    @GetMapping("/getAllOrders")
    public List<OrderDTO>getAllOrders(){
        return service.getAllOrders();

    }

    //update order record
    @PutMapping("/updateOrder/{id}")
    public boolean updateOrder(@RequestBody OrderDTO order,@PathVariable final Long id){
        return service.updateOrder(order,id);
    }


}
