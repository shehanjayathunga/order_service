package com.orderservice.orderservice.service;

import com.orderservice.orderservice.dto.OrderDTO;
import com.orderservice.orderservice.entities.OrderEntity;
import com.orderservice.orderservice.repository.OrderRepository;
import org.aspectj.weaver.ast.Or;
import org.slf4j.Logger;

import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import java.util.stream.Collectors;

@Service
public class OrderService {
    private final Logger LOGGER = LoggerFactory.getLogger(OrderService.class);


    @Autowired
    private OrderRepository repository;

    public List<OrderDTO>getOrderByUserId(Long id){
        List<OrderDTO>orders=null;
        try {
            orders = repository.findOrdersByUserId(id.toString())
                    .stream()
                    .map(order -> new OrderDTO(
                            order.getId(),
                            order.getOrderId(),
                            order.getUserId()
                    )).collect(Collectors.toList());
        }catch(Exception ex){
            LOGGER.warn("error "+ex);


        }
        return orders;
    }
    public boolean createOrder(OrderDTO order){
        try {
            OrderEntity orderEntity=new OrderEntity();
            orderEntity.setId(order.getId());
            orderEntity.setOrderId(order.getOrderId());
            orderEntity.setUserId(order.getUserId());
            repository.save(orderEntity);
            return true;

        }catch(Exception ex){

        }
        return false;
    }

    public List<OrderDTO>getAllOrders(){
        List<OrderDTO>orders=null;
        try {
            orders=repository.findAll().stream().map(OrderEntity->new OrderDTO(
                    OrderEntity.getId(),
                    OrderEntity.getOrderId(),
                    OrderEntity.getUserId()
            )).collect(Collectors.toList());

        }catch (Exception ex){
            LOGGER.warn("Exception in order service"+ex);

        }
        return orders;

    }

    public boolean updateOrder(OrderDTO order,Long id){
        try {
            OrderEntity orderEntity=new OrderEntity();
            orderEntity.setOrderId(order.getOrderId());
            orderEntity.setUserId(order.getUserId());
            orderEntity.setId(id);
            repository.save(orderEntity);
            return true;
        }catch (Exception ex){
            LOGGER.warn("Order service error:"+ex);
        }
        return false;

    }
}
